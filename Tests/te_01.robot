*** Settings ***
Library    SeleniumLibrary
Library    String

*** Variables ***
${url}=              https://the-internet.herokuapp.com/add_remove_elements/
${driver}=           chrome
${loc_add}=          xpath://button[@onclick='addElement()']
${loc_del}=          xpath://div[@id='elements']/button[_x_]


*** Test Cases ***
Test Crear
    [Documentation]    Create 20 elements on website and delete them after
    Open Browser       ${url}    ${driver}
    Maximize Browser Window
    Add Elements
    Delete Elements
    Close Browser


*** Keywords ***
Add Elements
    [Documentation]    Add elements to the website
    Wait Until Element Is Visible    ${loc_add}
    FOR    ${i}    IN RANGE    1       21
        ${x}=       convert to string    ${i}
        ${loc}=     Replace String      ${loc_del}      _x_     ${x}
        Click Element    ${loc_add}
        Wait Until Element Is Visible    ${loc}
    END
Delete Elements
    [Documentation]    Delete elements on the website
    FOR    ${i}    IN RANGE    20       0    -1
        ${x}=       convert to string    ${i}
        ${loc}=     Replace String      ${loc_del}      _x_     ${x}
        Wait Until Element Is Visible    ${loc}
        Click Element    ${loc}
        Wait Until Element Is Not Visible    ${loc}
    END