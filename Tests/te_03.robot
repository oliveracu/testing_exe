*** Settings ***
Library    SeleniumLibrary
Library    BuiltIn

*** Variables ***
${url}=              https://the-internet.herokuapp.com/disappearing_elements
${driver}=           chrome
${loc_gal}=          xpath://a[normalize-space()='Gallery']



*** Test Cases ***
Test Appear_Dissapear
    [Documentation]    Validate Gallery button appear and disappear from the website
    Open Browser       ${url}    ${driver}
    Maximize Browser Window
    Gallery Button Appear
    Gallery Button Disappear
    Close Browser


*** Keywords ***
Gallery Button Appear
    [Documentation]    Validate users are able to see the Gallery option
    ${Appear}=  Run Keyword And Return Status  Page Should Contain Element  ${loc_gal}
    WHILE    ${Appear} != ${TRUE}
        Reload Page
        ${Appear}=  Run Keyword And Return Status  Page Should Contain Element  ${loc_gal}
    END
    Element Should Be Visible    ${loc_gal}

Gallery Button Disappear
    [Documentation]    Validate users are not able to see the Gallery option
    ${Disappear}=  Run Keyword And Return Status  Page Should Not Contain Element    ${loc_gal}
    WHILE    ${Disappear} != ${TRUE}
        Reload Page
        ${Disappear}=  Run Keyword And Return Status  Page Should Not Contain Element    ${loc_gal}
    END
    Element Should Not Be Visible    ${loc_gal}