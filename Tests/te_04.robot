*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem


*** Variables ***
${url}=                https://the-internet.herokuapp.com/download
${driver}=             chrome
${loc_download}=       xpath://a[normalize-space()='some-file.txt']


*** Test Cases ***
Download Archivo
    [Documentation]    Download file and verify that it exist
    #Create directory and add specific changes to options
    ${driver_options}=  Evaluate  sys.modules['selenium.webdriver'].ChromeOptions()  sys, selenium.webdriver
    ${prefs}  Create Dictionary  download.default_directory=${CURDIR}\\Downloads
    Call Method  ${driver_options}  add_experimental_option  prefs  ${prefs}
    #Open driver with specific options to load
    Create Webdriver  Chrome  chrome_options=${driver_options}
    Goto  https://the-internet.herokuapp.com/download
    Click Link  css:[href="download/some-file.txt"]
    Sleep  5s
    File Should Exist    ${CURDIR}\\Downloads\\some-file.txt
    Close Browser

