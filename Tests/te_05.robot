*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem


*** Variables ***
${url}=              https://the-internet.herokuapp.com/upload
${driver}=           chrome
${loc_select}=       xpath://input[@id='file-upload']
${loc_upload}=       xpath://input[@id='file-submit']
${loc_header}=       xpath://h3[normalize-space()='File Uploaded!']
${loc_filename}=     xpath://div[@id='uploaded-files']

*** Test Cases ***
Crear un Archivo
    [Documentation]    Create file
    OperatingSystem.Create File    ${CURDIR}\\test.txt    testing
Subir Archivo
    [Documentation]    Upload file
    Open Browser       ${url}    ${driver}
    Maximize Browser Window
    Upload File And Verify
    Close Browser

*** Keywords ***
Upload File And Verify
    [Documentation]    Process upload of the file and verify it successfully was uploaded
    Choose File        ${loc_select}        ${CURDIR}\\test.txt
    Click Element      ${loc_upload}
    Element Text Should Be    ${loc_header}    File Uploaded!
    Element Text Should Be    ${loc_filename}    test.txt