*** Settings ***
Library    SeleniumLibrary
Library    String

*** Variables ***
${url}=                  https://the-internet.herokuapp.com/javascript_alerts
${driver}=               chrome
${loc_jsalert}=          xpath://button[@onclick='jsAlert()']
${loc_prompt}=           xpath://button[@onclick='jsPrompt()']
${loc_result}=           xpath://p[@id='result']


*** Test Cases ***
Js Alert and Prompt
    [Documentation]    Verify that Js Alert and Js Prompt buttons work
    Open Browser       ${url}    ${driver}
    Maximize Browser Window
    Js Alert
    Js Prompt
    Close Browser

*** Keywords ***
Js Alert
    [Documentation]    This would verify usage on Js alert button.
    Wait Until Element Is Visible    ${loc_jsalert}
    Click Element    ${loc_jsalert}
    Handle Alert    accept
    Element Text Should Be    ${loc_result}    You successfully clicked an alert
Js Prompt
    [Documentation]    This would verify the Js prompt button functionality.
    Wait Until Element Is Visible    ${loc_prompt}
    Click Element    ${loc_prompt}
    Input Text Into Alert    testing    accept
    Element Text Should Be    ${loc_result}    You entered: testing