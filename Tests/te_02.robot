*** Settings ***
Library    SeleniumLibrary


*** Variables ***
${url}=              https://the-internet.herokuapp.com/context_menu
${driver}=           chrome
${loc_linedbox}=     xpath://div[@id='hot-spot']


*** Test Cases ***
Validate Alert Message
    [Documentation]     This test case is to validate message showing on the alert pop up
    Open Browser       ${url}    ${driver}
    Maximize Browser Window
    Open Context Menu    ${loc_linedbox}
    Alert Should Be Present    You selected a context menu      ACCEPT
    Alert Should Not Be Present
    Close Browser

